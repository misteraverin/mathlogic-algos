from expression.axiom import *

fin = open("hw1.in", "r")
fout = open("hw1.out", "w")

str = fin.readline().rstrip()
print(str, file=fout)
list = str.split("|-")
assumptions = {}

if len(list) == 2:
    str = list[0].split(",")
    for i in range(len(str)):
        if str[i] != "":
            assumptions[parse_exp(str[i])] = i + 1
else:
    str = list

expressions = {}
list = []
current_line = 1


def print_line_number(cl):
    print("(", end="", file=fout)
    print(cl, end="", file=fout)
    print(") ", end="", file=fout)


def print_state(st, n):
    print(str, " ", end="", file=fout)
    if st == 0:
        print("(Не доказано)", sep="", file=fout)
    elif st == 1:
        print("(Предположение ", n, ")", sep="", file=fout)
    elif st == 2:
        print("(Сх. акс. ", n, ")", sep="", file=fout)
    else:
        first, second = n
        print("(М.Р. ", first, ", ", second, ")", sep="", file=fout)


def print_formated(cl, st, n):
    print_line_number(cl)
    print_state(st, n)


while True:
    str = fin.readline().rstrip()

    if not str:
        break

    expr = parse_exp(str)
    state = 0
    num = -1

    if expr in assumptions:
        state = 1
        num = assumptions[expr]
    if state == 0:
        for i in range(len(axiom_schemes)):
            if match_axiom(expr, axiom_schemes[i]):
                state = 2
                num = i + 1
                break
    if state == 0:
        for i in range(len(list) - 1, -1, -1):
            tmp = Impl(list[i], expr)

            if tmp in expressions:
                state = 3
                num = expressions[list[i]], expressions[tmp]
                break

    print_formated(current_line, state, num)

    if state != 0:
        expressions[expr] = current_line
        list.append(expr)

    current_line += 1

fin.close()
fout.close()
