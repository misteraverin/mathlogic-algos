from expression.expression import *


def normalize(string):
    ret = ""
    i = 0

    while i < len(string):
        if string[i].isdigit() or string[i].isalpha():
            ret += string[i]
            i += 1
        elif string[i] == "-":
            ret += " -> "
            i += 2
        else:
            ret += " " + string[i] + " "
            i += 1

    return ret


def parse_exp(expression):
    expression = normalize(expression)
    args = expression.split()
    ret, expr = parse_impl(args)
    return ret


def parse_impl(expr):
    ret, expr = parse_disj(expr)

    while len(expr) > 0 and expr[0] == "->":
        tmp, expr = parse_impl(expr[1:])
        ret = Impl(ret, tmp)

    return ret, expr


def parse_disj(expr):
    ret, expr = parse_conj(expr)

    while len(expr) > 0 and expr[0] == "|":
        tmp, expr = parse_conj(expr[1:])
        ret = Disj(ret, tmp)

    return ret, expr


def parse_conj(expr):
    ret, expr = parse_neg(expr)

    while len(expr) > 0 and expr[0] == "&":
        tmp, expr = parse_neg(expr[1:])
        ret = Conj(ret, tmp)

    return ret, expr


def parse_neg(expr):
    if expr[0] == "!":
        ret, expr = parse_neg(expr[1:])
        ret = Negate(ret)
    else:
        ret, expr = parse_unary(expr)

    return ret, expr


def parse_unary(expr):
    if expr[0] == "(":
        ret, expr = parse_impl(expr[1:])
    else:
        ret = Variable(expr[0])
    return ret, expr[1:]
