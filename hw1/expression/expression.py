mod = (10 ** 9) + 9


class Expression(object):
    hash = 0

    def __hash__(self):
        return self.hash

    def __eq__(self, other):
        return self.__hash__() == other.__hash__()


class Unary(Expression):
    def __init__(self, data):
        self.val = data


class Variable(Unary):
    def __init__(self, value):
        super().__init__(value)
        self.hash = value.__hash__() % mod

    def __str__(self):
        return self.val


class Negate(Unary):
    name = "!"

    def __init__(self, value):
        super().__init__(value)
        self.hash = (value.__hash__() * 3 + 7 * self.name.__hash__()) % mod

    def __str__(self):
        return "(! " + self.val.__str__() + " )"


class Binary(Expression):
    def __init__(self, left, right):
        self.left = left
        self.right = right
        self.hash = (3 * self.name.__hash__() + 11 * left.__hash__() + 23 * right.__hash__()) % mod

    def __str__(self):
        return "( " + self.left.__str__() + self.name + self.right.__str__() + " )"


class Impl(Binary):
    name = " -> "

    def __init__(self, left, right):
        super().__init__(left, right)


class Conj(Binary):
    name = " & "

    def __init__(self, left, right):
        super().__init__(left, right)


class Disj(Binary):
    name = " | "

    def __init__(self, left, right):
        super().__init__(left, right)


def match(expr, axiom, dictionary):
    if type(axiom) is Variable:
        if axiom in dictionary:
            if dictionary[axiom] != expr:
                return False
            else:
                return True
        else:
            dictionary[axiom] = expr
            return True
    elif type(expr) is type(axiom):
        if type(axiom) is Negate:
            return match(expr.val, axiom.val, dictionary)
        else:
            sub = match(expr.left, axiom.left, dictionary)
            if sub:
                sub = match(expr.right, axiom.right, dictionary)

            return sub
    else:
        return False


def match_axiom(expr, axiom):
    return match(expr, axiom, {})
