from utils.axiom import *


def is_any_axiom(expr):
    for i in range(len(axioms_schemes)):
        if match_axiom(expr, axioms_schemes[i]):
            return True

    return False


def is_predicate_axiom(expr):
    if type(expr) is Impl and type(expr.left) is Conj \
            and type(expr.left.right) is UniversalQuantifier and type(expr.left.right.val) is Impl:
        if expr.left.right.var.val in get_free_variables(expr.right, dict(), set()) \
                and free_subtract(expr.right, expr.left.right.val.right,
                                  Variable(expr.left.right.var), dict(), dict()) \
                and free_subtract(expr.right, expr.left.left, Variable(expr.left.right.var), dict(),
                                  dict()) \
                and expr.right == expr.left.right.val.left:
            return True

    return False


def is_mp(expression, expression_list, expressions, check):
    for expr in range(len(expression_list)):
        if Impl(expression_list[len(expression_list) - expr - 1], expression) in expressions:
            check = 2, expression_list[len(expression_list) - expr - 1]
            break

    return check


def is_universal(expr, expressions, free_variables, check, error_string):
    if type(expr) is Impl and type(expr.right) is UniversalQuantifier:
        tmp = Impl(expr.left, expr.right.val)
        if tmp in expressions:
            if expr.right.var.val not in get_free_variables(expr.left, dict(), set()):
                if expr.right.var.val not in free_variables:
                    check = 3, tmp
                else:
                    error_string = "[Невозможно переделать доказательство. Применение правил с кватором " \
                                   "всеобщности, используещее свободную переменную " \
                                   + expr.right.var.__str__() + " из предположений.]"
            else:
                error_string = "[Ошибка применения правил вывода с квантором всеобщности. Переменная " \
                               + expr.right.var.__str__() + " входит свободно.]"

    return check, error_string


def is_exists(expr, expressions, free_variables, check, error_string):
    if type(expr) is Impl and type(expr.left) is ExistsQuantifier:
        tmp = Impl(expr.left.val, expr.right)
        if tmp in expressions:
            if expr.left.var.val not in get_free_variables(expr.right, dict(), set()):
                if expr.left.var.val not in free_variables:
                    check = 4, tmp
                else:
                    error_string = "[Невозможно переделать доказательство. Применение правил с кватором " \
                                   "существования, используещее свободную переменную " \
                                   + expr.left.var.__str__() + " из предположений.]"
            else:
                error_string = "[Ошибка применения правил вывода с квантором существования. Переменная " \
                               + expr.right.var.__str__() + "входит свободно.]"

    return check, error_string


def subtract(expr, values):
    if type(expr) is Variable:
        return expr
    elif type(expr) is Predicate:
        if expr.name in values:
            return values[expr.name]
        for i in range(len(expr.val)):
            expr[i] = subtract(expr.val[i], values)
    elif isinstance(expr, Unary):
        expr.val = subtract(expr.val, values)
    else:
        expr.left = subtract(expr.left, values)
        expr.right = subtract(expr.right, values)

    if (type(expr) is UniversalQuantifier or type(expr) is ExistsQuantifier) and expr.var.val == "x":
        expr.var = values["x"].val
    expr.__hash__()

    return expr


def make_expr(parser, string, values):
    return subtract(parser.parse_exp(string), values)


def free_subtract(template, exp, var, locked, dictionary):
    if type(template) is Variable:
        if template != var:
            return template == exp
        if template.val in locked:
            return template == exp
        else:
            if template in dictionary:
                return dictionary[template] == exp
            else:
                tmp = set()
                get_free_variables(exp, dict(), tmp)

                if len(tmp.intersection(locked)) != 0:
                    return False
                dictionary[template] = exp

                return True
    elif type(template) is type(exp):
        if type(template) is UniversalQuantifier or type(template) is ExistsQuantifier:
            if template.var.val not in locked:
                locked[template.var.val] = 1
            else:
                locked[template.var.val] += 1

            result = free_subtract(template.val, exp.val, var, locked, dictionary)
            locked[template.var.val] -= 1

            if locked[template.var.val] == 0:
                locked.pop(template.var.val, None)

            return result
        elif type(template) is Predicate:
            if len(template.val) != len(exp.val):
                return False

            for i in range(len(template.val)):
                if not free_subtract(template.val[i], exp.val[i], var, locked, dictionary):
                    return False

            return True
        elif isinstance(template, Unary):
            return free_subtract(template.val, exp.val, var, locked, dictionary)
        else:
            if not free_subtract(template.left, exp.left, var, locked, dictionary):
                return False

            return free_subtract(template.right, exp.right, var, locked, dictionary)
    else:
        return False


def is_universal_axiom(expr):
    if type(expr) is not Impl or type(expr.left) is not UniversalQuantifier:
        return False

    return free_subtract(expr.left.val, expr.right, expr.left.var, dict(), dict())


def is_exists_axiom(expr):
    if type(expr) is not Impl or type(expr.right) is not ExistsQuantifier:
        return False

    return free_subtract(expr.right.val, expr.left, expr.right.var, dict(), dict())


def is_any_formal_axiom(expr):
    for axiom in formal_axioms_schemes:
        if match_subst(axiom, expr, set(), dict()):
            return True

    if is_universal_axiom(expr) or is_exists_axiom(expr):
        return True

    return False
