mod = (10 ** 9) + 9


class Expression(object):
    hash = 0

    def __hash__(self):
        return self.hash

    def __eq__(self, other):
        return self.__hash__() == other.__hash__()


class Unary(Expression):
    def __init__(self, value):
        self.val = value


class Variable(Unary):
    def __init__(self, value):
        super().__init__(value)
        self.hash = value.__hash__() % mod

    def __str__(self):
        return self.val

    def evaluate(self, dictionary):
        return dictionary[self.val]


class Negate(Unary):
    name = "!"

    def __init__(self, value):
        super().__init__(value)
        self.hash = (3 * self.val.__hash__() ** 2 + 7 * self.name.__hash__()) % mod

    def __str__(self):
        return "(!" + self.val.__str__() + ")"

    def rehash(self):
        self.hash = (3 * self.val.__hash__() ** 2 + 7 * self.name.__hash__()) % mod

    def evaluate(self, dictionary):
        return not self.val.evaluate(dictionary)


class Binary(Expression):
    def __init__(self, left, right):
        self.left = left
        self.right = right
        self.hash = (3 * self.name.__hash__() + 11 * self.left.__hash__() ** 3 + 23 * self.right.__hash__() ** 4) % mod

    def __str__(self):
        return "(" + self.left.__str__() + self.name + self.right.__str__() + ")"

    def calculate(self, left, right):
        raise NotImplementedError

    def evaluate(self, dictionary):
        return self.calculate(self.left.evaluate(dictionary), self.right.evaluate(dictionary))


class Impl(Binary):
    name = "->"

    def __init__(self, left, right):
        super().__init__(left, right)

    def calculate(self, left, right):
        return (not left) or right


class Conj(Binary):
    name = "&"

    def __init__(self, left, right):
        super().__init__(left, right)

    def calculate(self, left, right):
        return left and right


class Disj(Binary):
    name = "|"

    def __init__(self, left, right):
        super().__init__(left, right)

    def calculate(self, left, right):
        return left or right


class UniversalQuantifier(Unary):
    name = "@"

    def __init__(self, var, value):
        super().__init__(value)
        self.var = var
        self.hash = (17 * self.var.__hash__() ** 3 + 3 * self.val.__hash__() ** 2 + 7 * self.name.__hash__()) % mod

    def __str__(self):
        return "(@" + self.var.__str__() + self.val.__str__() + ")"


class ExistsQuantifier(Unary):
    name = "?"

    def __init__(self, var, value):
        super().__init__(value)
        self.var = var
        self.hash = (17 * self.var.__hash__() ** 3 + 3 * self.val.__hash__() ** 2 + 7 * self.name.__hash__()) % mod

    def __str__(self):
        return "(?" + self.var.__str__() + self.val.__str__() + ")"


class Predicate(Unary):
    def __init__(self, name, values):
        super().__init__(values)
        self.name = name
        self.hash = self.name.__hash__() % mod
        for exp in self.val:
            self.hash *= 2017
            self.hash += exp.__hash__()

    def __str__(self):
        if len(self.val) == 0:
            return self.name

        ret = self.name + "("

        for i in range(len(self.val)):
            if i == len(self.val) - 1:
                ret += self.val[i].__str__() + ")"
            else:
                ret += self.val[i].__str__() + ","

        return ret


class Next(Unary):
    name = "'"

    def __init__(self, value):
        super().__init__(value)
        self.hash = (self.val.__hash__() * 31 + self.name.__hash__()) % mod

    def __str__(self):
        return self.val.__str__() + self.name


class Equals(Binary):
    def calculate(self, left, right):
        pass

    name = "="

    def __init__(self, left, right):
        super().__init__(left, right)


class Add(Binary):
    name = "+"

    def __init__(self, left, right):
        super().__init__(left, right)


class Mul(Binary):
    name = "*"

    def __init__(self, left, right):
        super().__init__(left, right)


def match(exp, axiom, dictionary):
    if type(axiom) is Variable:
        if axiom in dictionary:
            return dictionary[axiom] == exp
        else:
            dictionary[axiom] = exp
            return True
    elif type(exp) is type(axiom):
        if isinstance(axiom, Unary):
            return match(exp.val, axiom.val, dictionary)
        else:
            sub = match(exp.left, axiom.left, dictionary)

            if sub:
                sub = match(exp.right, axiom.right, dictionary)

            return sub
    else:
        return False


def match_axiom(exp, checking_axiom):
    return match(exp, checking_axiom, {})


def get_variables(exp, dictionary):
    if type(exp) is Variable:
        if exp.val not in dictionary:
            dictionary[exp.val] = len(dictionary)
    elif isinstance(exp, Unary):
        get_variables(exp.val, dictionary)
    else:
        get_variables(exp.left, dictionary)
        get_variables(exp.right, dictionary)


def get_free_variables(exp, dictionary, result):
    if type(exp) is Variable:
        if exp not in dictionary.keys():
            result.add(exp.val)
    elif type(exp) is Predicate:
        for e in exp.val:
            get_free_variables(e, dictionary, result)
    elif type(exp) is UniversalQuantifier or type(exp) is ExistsQuantifier:
        if exp.var in dictionary.keys():
            dictionary[exp.var] += 1
        else:
            dictionary[exp.var] = 1

        get_free_variables(exp.val, dictionary, result)
        dictionary[exp.var] -= 1

        if dictionary[exp.var] == 0:
            dictionary.pop(exp.var)
    elif isinstance(exp, Unary):
        get_free_variables(exp.val, dictionary, result)
    else:
        get_free_variables(exp.left, dictionary, result)
        get_free_variables(exp.right, dictionary, result)

    return result


def match_subst(template, exp, locked, dictionary):
    if type(template) is Variable:
        if template in locked:
            return template == exp
        else:
            if template in dictionary:
                return dictionary[template] == exp
            else:
                dictionary[template] = exp
                return True
    elif type(template) is type(exp):
        if type(template) is UniversalQuantifier or type(template) is ExistsQuantifier:
            locked.add(template.var)
            result = match_subst(template.val, exp.val, locked, dictionary)

            locked.remove(template.var)
            return result
        elif type(template) is Predicate:
            if len(template.val) != len(exp.val):
                return False
            for i in range(template.val):
                if not match_subst(template.val[i], exp.val[i], locked, dictionary):
                    return False

            return True
        elif isinstance(template, Unary):
            return match_subst(template.val, exp.val, locked, dictionary)
        else:
            if not match_subst(template.left, exp.left, locked, dictionary):
                return False

            return match_subst(template.right, exp.right, locked, dictionary)
    else:
        return False


def is_valid_formula(exp):
    temp = dict()
    get_variables(exp, temp)
    dictionary = dict()

    for e in temp:
        dictionary[temp[e]] = e

    for i in range(0, 2 ** len(dictionary)):
        temp = dict()
        for j in range(0, len(dictionary)):
            if i & (2 ** j) == 0:
                temp[dictionary[j]] = False
            else:
                temp[dictionary[j]] = True

        if not exp.evaluate(temp):
            return temp

    return dict()
