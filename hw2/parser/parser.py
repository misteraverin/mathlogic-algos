from expression.expression import *


def normalize(string):
    ret = ""
    i = 0

    while i < len(string):
        if string[i].isdigit() or string[i].isalpha():
            ret += string[i]
            i += 1
        elif string[i] == "-":
            ret += " -> "
            i += 2
        else:
            ret += " " + string[i] + " "
            i += 1

    return ret


def parse_exp(expression):
    expression = normalize(expression)
    args = expression.split()
    ret, expr = parse_impl(args)

    return ret


def parse_impl(expr):
    ret, expr = parse_disj(expr)

    while len(expr) > 0 and expr[0] == "->":
        tmp, expr = parse_impl(expr[1:])
        ret = Impl(ret, tmp)

    return ret, expr


def parse_disj(expr):
    ret, expr = parse_conj(expr)

    while len(expr) > 0 and expr[0] == "|":
        tmp, expr = parse_conj(expr[1:])
        ret = Disj(ret, tmp)

    return ret, expr


def parse_conj(expr):
    ret, expr = parse_neg(expr)

    while len(expr) > 0 and expr[0] == "&":
        tmp, expr = parse_neg(expr[1:])
        ret = Conj(ret, tmp)

    return ret, expr


def parse_neg(expr):
    if expr[0] == "!":
        ret, expr = parse_neg(expr[1:])
        ret = Negate(ret)
    else:
        ret, expr = parse_unary(expr)

    return ret, expr


def parse_unary(expr):
    if expr[0] == "(":
        ret, expr = parse_impl(expr[1:])
    else:
        ret = Variable(expr[0])
    return ret, expr[1:]


class FormalParser:
    def __init__(self):
        self.expression = ""
        self.index = 0

    def parse_exp(self, string):
        self.index = 0
        self.expression = string

        return self.parse_impl()

    def parse(self):
        if self.index >= len(self.expression):
            return None

        return self.parse_impl()

    def get_var_name(self):
        index = self.index

        while index < len(self.expression) and (self.expression[index].isdigit()
                                                or (self.expression[index].isalpha()
                                                    and self.expression[index].islower())):
            index += 1

        ret = self.expression[self.index:index]
        self.index = index

        return ret

    def get_predicate_name(self):
        index = self.index

        if not (self.expression[index].isalpha() and self.expression[index].isupper()):
            return ""

        while index < len(self.expression) and (self.expression[index].isdigit()
                                                or (self.expression[index].isalpha()
                                                    and self.expression[index].isupper())):
            index += 1

        ret = self.expression[self.index:index]
        self.index = index

        return ret

    def parse_impl(self):
        ret = self.parse_disj()

        while self.index < len(self.expression) and self.expression[self.index] == '-':
            self.index += 2
            tmp = self.parse_impl()
            ret = Impl(ret, tmp)

        return ret

    def parse_disj(self):
        ret = self.parse_conj()

        while self.index < len(self.expression) and self.expression[self.index] == '|':
            self.index += 1
            tmp = self.parse_conj()
            ret = Disj(ret, tmp)

        return ret

    def parse_conj(self):
        ret = self.parse_unary()

        while self.index < len(self.expression) and self.expression[self.index] == '&':
            self.index += 1
            tmp = self.parse_unary()
            ret = Conj(ret, tmp)

        return ret

    def parse_unary(self):
        if self.expression[self.index] == '!':
            self.index += 1
            tmp = self.parse_unary()

            return Negate(tmp)
        elif self.expression[self.index] == '@' or self.expression[self.index] == '?':
            sym = self.expression[self.index]
            self.index += 1
            var = self.get_var_name()
            tmp = self.parse_unary()

            if sym == '@':
                return UniversalQuantifier(Variable(var), tmp)
            else:
                return ExistsQuantifier(Variable(var), tmp)

        ret = self.parse_predicate()
        if not (ret is None):
            return ret

        if self.index < len(self.expression) and self.expression[self.index] == '(':
            self.index += 1
            ret = self.parse_impl()
            self.index += 1
            return ret

        temp = self.get_var_name()
        return Variable(temp)

    def parse_predicate(self):
        predicate = self.get_predicate_name()
        if predicate != "":
            args = self.parse_args()
            return Predicate(predicate, args)
        else:
            index = self.index
            result = self.parse_term()

            if self.index >= len(self.expression) or self.expression[self.index] != '=':
                self.index = index
                return None

            self.index += 1
            return Equals(result, self.parse_term())

    def parse_args(self):
        ret = list()

        if self.index >= len(self.expression) or self.expression[self.index] != '(':
            return ret

        self.index += 1
        ret.append(self.parse_term())

        while self.index < len(self.expression) and self.expression[self.index] != ')':
            self.index += 1
            ret.append(self.parse_term())

        self.index += 1
        return ret

    def parse_term(self):
        ret = self.parse_add()

        while self.index < len(self.expression) and self.expression[self.index] == '+':
            self.index += 1
            tmp = self.parse_add()
            ret = Add(ret, tmp)

        return ret

    def parse_add(self):
        ret = self.parse_mul()

        while self.index < len(self.expression) and self.expression[self.index] == '*':
            self.index += 1
            tmp = self.parse_mul()
            ret = Mul(ret, tmp)

        return ret

    def parse_mul(self):
        if self.index < len(self.expression) and self.expression[self.index] == '(':
            self.index += 1
            result = self.parse_term()
            self.index += 1

            return self.parse_next(result)

        var = self.get_var_name()

        if self.index < len(self.expression) and self.expression[self.index] == '(':
            args = self.parse_args()
            result = Predicate(var, args)
        else:
            result = Variable(var)

        return self.parse_next(result)

    def parse_next(self, val):
        while self.index < len(self.expression) and self.expression[self.index] == '\'':
            self.index += 1
            val = Next(val)

        return val
