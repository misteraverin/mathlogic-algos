from utils.proof import *
from utils.utils import *


def solve():
    fin = open("hw2.in", "r")
    fout = open("hw2.out", "w")

    parser = FormalParser()
    line, expression = fin.readline().rstrip().split("|-")
    assumptions = set()
    parser.expression = line
    free_variables = set()
    last_assumption = None

    while parser.index < len(parser.expression):
        tmp = parser.parse()
        last_assumption = tmp
        assumptions.add(tmp)

        if parser.index < len(parser.expression) and parser.expression[parser.index] == ',':
            parser.index += 1

    if len(assumptions) > 0:
        get_free_variables(last_assumption, dict(), free_variables)
    parser.index += 2

    expression = parser.parse_exp(expression)
    expressions = set()
    expression_list = list()
    prior = list()

    if not is_correct(assumptions, expression_list, expressions, fin, fout, free_variables, parser, prior):
        return

    print("Вывод корректен")
    semicolon = 0

    for e in assumptions:
        if e != last_assumption:
            print(e, sep="", end="", file=fout)
            if semicolon < len(assumptions) - 2:
                print(",", sep="", end="", file=fout)
            semicolon += 1

    if len(assumptions) > 0:
        print("|-", Impl(last_assumption, expression), sep="", end="\n", file=fout)

        universal_proof = universal
        exists_proof = exists

        print_proof(exists_proof, expression_list, last_assumption, fout, parser, prior, universal_proof)
    else:
        print("|-", expression, sep="", file=fout)
        for expr in expression_list:
            print(expr, file=fout)

    fin.close()
    fout.close()


def is_correct(assumptions, expression_list, expressions, fin, fout, free_variables, parser, prior):
    line_number = 0

    while True:
        line_number += 1
        if line_number % 100 == 0:
            print("Обработано ", line_number, " строк.")
        error_string = "[Недоказаное утверждение]"

        line = fin.readline().rstrip()
        if not line:
            break

        expression = parser.parse_exp(line)
        check = -1, None

        if is_any_axiom(expression) \
                or is_any_formal_axiom(expression) \
                or is_predicate_axiom(expression):
            check = 0, None

        if check[0] == -1 and expression in assumptions:
            check = 1, None

        if check[0] == -1:
            check = is_mp(expression, expression_list, expressions, check)

        if check[0] == -1:
            check, error_string = is_universal(expression, expressions, free_variables, check, error_string)

        if check[0] == -1:
            check, error_string = is_exists(expression, expressions, free_variables, check, error_string)

        if check[0] == -1:
            print("Вывод не корректен")
            print("Вывод некорректен, начиная с формулы №", line_number, ":", error_string, "\n", line, end="\n",
                  file=fout)

            return False
        else:
            expressions.add(expression)
            expression_list.append(expression)
            prior.append(check)

    return True


def print_proof(exists_proof, expression_list, last_assumption, output, parser, prior, universal_proof):
    for i in range(len(expression_list)):
        if prior[i][0] == 0:
            print_axiom(expression_list, output, i, last_assumption)
        elif prior[i][0] == 1:
            if expression_list[i] != last_assumption:
                print_axiom(expression_list, output, i, last_assumption)
            else:
                print_assumption(output, last_assumption)
        elif prior[i][0] == 2:
            print_mp(expression_list, output, i, last_assumption, prior)
        elif prior[i][0] == 3:
            print_universal_quantifier(expression_list, output, i, last_assumption, parser, universal_proof)
        elif prior[i][0] == 4:
            print_exists_quantifier(exists_proof, expression_list, output, i, last_assumption, parser)


def print_exists_quantifier(exists_proof, expression_list, fout, i, last_assumption, parser):
    m = {"A": last_assumption, "B": expression_list[i].left.val, "C": expression_list[i].right,
         "x": expression_list[i].left.var}

    for str in exists_proof:
        print(make_expr(parser, str, m), file=fout)


def print_universal_quantifier(expression_list, fout, i, last_assumption, parser, universal_proof):
    m = {"A": last_assumption, "B": expression_list[i].left, "C": expression_list[i].right.val,
         "x": expression_list[i].right.var}

    for i in range(len(universal_proof)):
        str = universal_proof[i]
        tmp = make_expr(parser, str.rstrip(), m)
        print(tmp, file=fout)


def print_mp(expression_list, fout, i, last_assumption, prior):
    tmp = Impl(last_assumption, Impl(prior[i][1], expression_list[i]))
    tmp1 = Impl(last_assumption, prior[i][1])
    tmp2 = Impl(last_assumption, expression_list[i])

    print(Impl(tmp1, Impl(tmp, tmp2)), file=fout)
    print(Impl(tmp, tmp2), file=fout)
    print(tmp2, file=fout)


def print_assumption(fout, last_assumption):
    tmp1 = Impl(last_assumption, Impl(last_assumption, last_assumption))
    tmp2 = Impl(last_assumption, Impl(Impl(last_assumption, last_assumption), last_assumption))
    tmp3 = Impl(last_assumption, last_assumption)

    print(tmp1, file=fout)
    print(Impl(tmp1, Impl(tmp2, tmp3)), file=fout)
    print(Impl(tmp2, tmp3), file=fout)
    print(tmp2, file=fout)
    print(tmp3, file=fout)


def print_axiom(expression_list, fout, i, last_assumption):
    print(Impl(expression_list[i], Impl(last_assumption, expression_list[i])), file=fout)
    print(expression_list[i], file=fout)
    print(Impl(last_assumption, expression_list[i]), file=fout)


solve()
