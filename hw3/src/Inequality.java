import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.Scanner;

public class Inequality {
    public static void main(String[] args) {
        Scanner reader;
        PrintWriter writer;
        try {
            reader = new Scanner(new FileReader("hw3.in"));
            writer = new PrintWriter(new File("hw3.out"));
        } catch (FileNotFoundException e) {
            System.err.println("File not found.");
            return;
        }

        int a = reader.nextInt();
        int b = reader.nextInt();
        int c = b - a;

        if (c >= 0) {
            StringBuilder aa = new StringBuilder("0");
            StringBuilder bb = new StringBuilder("0");
            StringBuilder cc = new StringBuilder("0");

            for (int i = 0; i < b; i++) {
                if (i < c) {
                    cc.append("'");
                }
                if (i < a) {
                    aa.append("'");
                }
                bb.append("'");
            }

            writer.println("|-?p(" + aa.toString() + "+p)=" + bb.toString());
            try {
                reader = new Scanner(new FileReader("Base"));
            } catch (FileNotFoundException e) {
                System.err.println("Base not found");
                return;
            }
            while (reader.hasNextLine()) {
                writer.println(reader.nextLine());
            }

            writer.println("@a(a+0=a)->" + aa.toString() + "+0=" + aa.toString());
            writer.println(aa.toString() + "+0=" + aa.toString());

            StringBuilder proof = new StringBuilder();
            String s;
            try {
                reader = new Scanner(new FileReader("Proof"));
            } catch (FileNotFoundException e) {
                System.err.println("Proof not found.");
                return;
            }
            while (reader.hasNextLine()) {
                s = reader.nextLine();
                proof.append(s.replaceAll("A", aa.toString())).append("\n");
            }

            StringBuilder inc = new StringBuilder();
            for (int i = 0; i < c; i++) {
                writer.print(proof.toString().replaceAll("#", inc.toString()));
                inc.append("'");
            }
            writer.println("(" + aa.toString() + "+" + cc.toString() + ")="
                    + bb.toString() + "->?p(" + aa.toString() + "+p)="
                    + bb.toString() + "\n" + "?p(" + aa.toString() + "+p)="
                    + bb.toString());
        } else {
            try {
                reader = new Scanner(new FileReader("Simple"));
            } catch (FileNotFoundException e) {
                System.err.println("Simple not found.");
                return;
            }
            while (reader.hasNextLine()) {
                writer.println(reader.nextLine());
            }

            c = a - b;
            StringBuilder cc = new StringBuilder("0");
            StringBuilder bb = new StringBuilder("0");
            for (int i = 0; i < c - 1; i++) {
                if (i < b) {
                    bb.append("'");
                }
                cc.append("'");
            }

            if (c - 1 < b) {
                for (int i = c - 1; i < b; i++) {
                    bb.append("'");
                }
            }

            try {
                reader = new Scanner(new FileReader("ContinueProof"));
            } catch (FileNotFoundException e) {
                System.err.println("ContinueProof no found.");
                return;
            }
            while (reader.hasNextLine()) {
                writer.println(reader.nextLine().replaceAll("C", cc.toString())
                        .replaceAll("B", bb.toString()).replaceAll(" ", ""));
            }
        }

        reader.close();
        writer.close();
    }
}
