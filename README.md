# Mathlogis course
This repo contains HWs for MathLogic course at ITMO UNIVERSITY Autumn 2016.
Homeworks are mostly parsers and processors of proofs of expressions.

**Classical Logic Proof Checker** (HW1) gets a file with Hilbert-style proof and checks, if its correct. 
Proof Checker has parser inside it, which builds Abstract Syntax Tree and then checks its structure.

**Predicate Calculus Proof Processor** (HW2) is a tool for working with proofs of expressions in 
formal arithmetics and in Predicate Calculus. It is an extension of first.